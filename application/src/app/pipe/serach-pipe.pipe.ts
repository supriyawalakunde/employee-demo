import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../model/employee';

@Pipe({
  name: 'serachPipe'
})
export class SerachPipePipe implements PipeTransform {

  transform(items:Employee[], value: string): Employee[] {
    if(!items){
      return [];
    }
    if(!value){
      return items;
    }
    
    let valueInLowercase=value.toLowerCase();
    return items.filter(i=>{
      return i.name.toLowerCase().includes(valueInLowercase) ||  i.designation.toLowerCase().includes(valueInLowercase)  || i.salary.toString().includes(valueInLowercase) 
    })

  }

}
