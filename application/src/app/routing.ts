import{ RouterModule, Routes} from '@angular/router';
import { HomeComponent} from './home/home.component';
import { FormComponent } from './form/form.component';
import {ErrorComponent} from './error/error.component';
import{ModuleWithProviders} from '@angular/core';

export const appRoutes:Routes=[
    {path:'home', component:HomeComponent},
    {path:'form', component:FormComponent},
    {path:'' , component:HomeComponent},
    {path:'**',component:ErrorComponent}
]

export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);