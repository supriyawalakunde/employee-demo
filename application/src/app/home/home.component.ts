import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../model/employee';
import { EmployeeService } from '../service/employee.service';
import { Popup} from 'ng2-opd-popup';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public app = "Sup";
  model: any;
  selectedEmployee:Employee;

  @ViewChild("popup")
  popup:Popup;

  constructor(private router: Router, private svc: EmployeeService) { }

  ngOnInit() {
    this.svc.getEmployees().subscribe(data => {
      this.model = data;
    });
    this.popup.options={
      header:"Are you sure?",
      confirmBtnContent: "Delete",
      confirmBtnClass:"btn btn-danger",
      cancleBtnClass:"btn btn-primary"
    }
  }

  showPopup(employee:Employee){
    this.selectedEmployee=employee;
    this.popup.show(this.popup.options);
  }

  hidePopup(){
    this.popup.hide();
    this.selectedEmployee=null;
  }

  deleteEmployee(){
    this.svc.deleteEmployees(this.selectedEmployee);
    this.hidePopup();
  }

  editEmployee(employee: Employee) {
    this.svc.setter(employee);
    this.router.navigate(['form']);
    console.log("edit")
  }

  

  addEmployee() {
    this.svc.setter(new Employee());
    this.router.navigate(['form']);
  }

}
