import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Employee }from '../model/employee';
import {of} from 'rxjs';

import {data} from '../model/employees'


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  emp:Employee[]=data;
  private employee:Employee;

  constructor(private http:HttpClient) { }

  getEmployees()
  {
   return of(data); 
  }

  setter(employee:Employee){
    this.employee=employee;
  }

  getter():Employee{
    return this.employee;
  }

  updateEmployees(employee:Employee)
  {
    let employeeOld=this.emp.find(e=>e.id===employee.id);
    let empIndex=this.emp.indexOf(employeeOld);
    this.emp[empIndex]=employee;
  }
 
  deleteEmployees(employee:Employee){
    let employeeOld=this.emp.find(e=>e.id===employee.id);
    let empIndex=this.emp.indexOf(employeeOld);
    this.emp.splice(empIndex,1);
  }

saveEmployees(employee:Employee){
  employee.id=this.emp.length+1;
  this.emp.push(employee);

}
  
}
