import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../model/employee';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public employee: Employee;
  option: string = "Save";

  constructor(private router: Router, private svc: EmployeeService) {
  }

  ngOnInit() {
    let empT=this.svc.getter() == null ? new Employee() : this.svc.getter();
    this.employee = new Employee;
    this.employee.id=empT.id;
    this.employee.designation=empT.designation;
    this.employee.name=empT.name;
    this.employee.salary=empT.salary
  }

  

  updateData() {
    this.router.navigate(['home']);
    this.svc.updateEmployees(this.employee);
  }

  cancelData() {
    this.svc.setter(null);
    
    this.router.navigate(['home']);
  }

  saveData() {
    this.router.navigate(['home']);
    this.svc.saveEmployees(this.employee);
  }

}
