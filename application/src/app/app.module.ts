import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PopupModule} from 'ng2-opd-popup';


import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { SerachPipePipe } from './pipe/serach-pipe.pipe';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { routing } from './routing';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    SerachPipePipe,
    HomeComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    PopupModule.forRoot(),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
