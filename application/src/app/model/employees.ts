import { Employee } from './employee'

export const data: Employee[] =
  [
    {
      id: 1,
      name: "Jack",
      designation: "Software Developer",
      salary: 50000
    },
    {
      id: 2,
      name: "James",
      designation: "Software tester",
      salary: 70000
    },
    {
      id: 3,
      name: "Jill",
      designation: "Automation tester",
      salary: 45000
    },
    {
      id: 4,
      name: "Max",
      designation: "Project Manager",
      salary: 90000
    },
    {
      id: 5,
      name: "Adom",
      designation: "CTO",
      salary: 100000
    },
  ]